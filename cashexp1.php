<html>
<body background="time.jpg">
<title>EXP-THROUGH-CASH</title>
<head>
<style>
.button {
  border-radius: 10px;
  background-color: #f4511e;
  border: 2;
  color: #FFFFFF;
  text-align: center;
  font-size: 22px;
  padding: 11px;
  width: 120px;
  transition: all 0.5s;
  cursor: pointer;
  margin: 5px;
}

.button span {
  cursor: pointer;
  display: inline-block;
  position: relative;
  transition: 0.5s;
}

.button span:after {
  content: '»';
  position: absolute;
  opacity: 0;
  top: 0;
  right: -20px;
  transition: 0.5s;
}

.button:hover span {
  padding-right: 25px;
}

.button:hover span:after {
  opacity: 1;
  right: 0;
}
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 2px solid #dddddd;
    text-align: left;
    padding: 8px;
}
tr:nth-child(even) {
    background-color: #dddddd;
}
</style>

</head>
<body text="black">

<h1><center>Expenditure-through-Cash Entry Form</h1>
<h3><center><marquee style="height:30;width:400" scrollamount="50" scrolldelay="450"> Please use 'Google Chrome' for entry...</marquee></h3>
<?php
session_start();
$n=$_SESSION[nm];
$ne=$_SESSION[nme];
echo "<h2>Welcome, ".$ne."<br/>";

$dbpwd=$_SESSION[dbpd];
$rt=$_SESSION[rt];
$lh=$_SESSION[lh];
$db=$_SESSION[db];

//$link = @mysql_pconnect($lh,$rt,$dbpwd) or exit();
//mysql_select_db($db) or exit();

?>

<table border=2>Enter Expenses through Cash
<form action="cshexp2.php" method="post"
<tr><th>Date of Transaction:</th><td><input type="date" name="dt" min="01/01/2016"> Select Date</td></tr> 

<tr><th>Rent :</th><td><input type="text" name="rent" /> </td></tr> 
<tr><th>Food :</th><td><input type="text" name="food" /> </td></tr> 
<tr><th>Clothings :</th><td><input type="text" name="cloth" /> </td></tr> 
<tr><th>Education fees :</th><td><input type="text" name="fees" /> </td></tr> 
<tr><th>Transportation :</th><td><input type="text" name="commn" /> </td></tr> 
<tr><th>Books Purchase :</th><td><input type="text" name="books" /> </td></tr> 
<tr><th>Library fees :</th><td><input type="text" name="lib" /> </td></tr> 
<tr><th>Mobile/Phone charges :</th><td><input type="text" name="mobile" /> </td></tr> 
<tr><th>Entertainment/ Misc :</th><td><input type="text" name="misc" /> </td></tr> 
</table>
<?php echo '<p>Check once again for correctness</p>';?>
<button class="button"><span>SAVE</span></button>

</form>
<br>
<A Href="first.php">MAIN MENU</A>
</body>
</html>