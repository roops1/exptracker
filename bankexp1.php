<html>
<head>
<title>EXP-THROUGH-BANK</title>
<style>
.button {
  border-radius: 10px;
  background-color: #f4511e;
  border: 2;
  color: #FFFFFF;
  text-align: center;
  font-size: 12px;
  padding: 9px;
  width: 110px;
  transition: all 0.5s;
  cursor: pointer;
  margin: 5px;
}

.button span {
  cursor: pointer;
  display: inline-block;
  position: relative;
  transition: 0.5s;
}

.button span:after {
  content: '»';
  position: absolute;
  opacity: 0;
  top: 0;
  right: -20px;
  transition: 0.5s;
}

.button:hover span {
  padding-right: 25px;
}

.button:hover span:after {
  opacity: 1;
  right: 0;
}
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 80%;
}

td, th {
    border: 3px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(odd) {
    background-color: #dddddd;
}
tr:nth-child(even)
{
background-color:#A9A9A9;
}
</style>
</head>
<body background="3.jpg">

<body text="black">

<h1><center><font color="white">Expenditure Entry Form</font></h1>
<h3><center><marquee style="height:30;width:400" scrollamount="50" scrolldelay="450"><font color="white"> Please use 'Google Chrome' for entry...</marquee></font></h3>
<?php
session_start();
$n=$_SESSION[nm];
$ne=$_SESSION[nme];
//echo "<h3> Welcome, ".$ne."<br/>";

$dbpwd=$_SESSION[dbpd];
$rt=$_SESSION[rt];
$lh=$_SESSION[lh];
$db=$_SESSION[db];

//$link = @mysql_pconnect($lh,$rt,$dbpwd) or exit();
//mysql_select_db($db) or exit();

?>

<table align="center"> <h3><font color="white">Enter Expenses</font>
<form action="bnkexp2.php" method="post"
<tr><th>Date of Transaction:</th><th><input type="date" name="dt" min="01/01/2016"> Select Date</th></tr> 
<tr><th>Description::</th><td><input type="text" name="descn"/></td>
<tr><th>Rent :</th><td><input type="text" name="rent" /> </td></tr> 
<tr><th>Food :</th><td><input type="text" name="food" /> </td></tr> 
<tr><th>Clothing :</th><td><input type="text" name="cloth" /> </td></tr> 
<tr><th>Education fees :</th><td><input type="text" name="fees" /> </td></tr> 
<tr><th>Transportation :</th><td><input type="text" name="commn" /> </td></tr> 
<tr><th>Books Purchase :</th><td><input type="text" name="books" /> </td></tr> 
<tr><th>Library fees :</th><td><input type="text" name="lib" /> </td></tr> 
<tr><th>Mobile/Phone charges :</th><td><input type="text" name="mobile" /> </td></tr> 
<tr><th>Entertainment/ Misc :</th><td><input type="text" name="misc" /> </td></tr> 
</table>
<h3><font color="white">Check once again for correctness</font><br/>
<button class="button"><span>SAVE.</span></button>
</form>
<form action="first.php" method="post">
<button class="button"><span>MAIN MENU</span></button>
</form>
</body>
</html>